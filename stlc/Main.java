import ast.*;

public class Main {
	public static void main(String args[]) {
		/**
 		 * let x = 3 in
		 * let f = fun(x : num) { x } in
		 * f x
		 */
		// Build bottom-up
		App a1 = new App(
			new IdExpr(new IdUse("f")),
			new IdExpr(new IdUse("x")));
		Fun f1 = new Fun(
			new IdDecl("x"),
			new NumType(),
			new IdExpr(new IdUse("x")));
		Let l1 = new Let(
			new IdDecl("f"),
			f1,
			a1);
		Let l2 = new Let(
			new IdDecl("x"),
			new Literal(3),
			l1);
		Program p = new Program(l2);

		System.out.println(p);
		System.out.println(l2.type());
		System.out.println(l1.type());
	}
}
