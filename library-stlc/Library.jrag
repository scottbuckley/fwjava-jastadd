import java.util.*;
import java.util.function.Supplier;

aspect ScopeLibrary {
  interface IDeclaration {
  }
  interface IUse {
  }
  class Scope {
  }

  syn String IDeclaration.name() = getName();
  syn String IUse.name() = getName();


  private Map<String, IDeclaration> Scope.bindings = new HashMap<>();
  public Scope Scope.d(IDeclaration ...ds) {
    for (IDeclaration d: ds) {
      if (d != null && !bindings.containsKey(d.name())) {
        bindings.put(d.name(), d);
      }
    }
    return this;
  }
  syn IDeclaration ASTNode.getDeclaration() = null;
  public Scope Scope.d(List<? extends ASTNode> list) {
    for (ASTNode<?> e: list) {
      d(e.getDeclaration());
    }
    return this;
  }
  public Scope Scope.d(Scope s) {
    for (IDeclaration d: s.bindings.values()) {
      d(d);
    }
    return this;
  }
  private java.util.List<Supplier<Scope>> Scope.scopes = new ArrayList<>();
  public Scope Scope.s(Supplier<Scope> ...sups) {
    for (Supplier<Scope> sup: sups) {
      if (sup != null) {
        scopes.add(ASTNode.cachedSupplier(sup));
      }
    }
    return this;
  }
  public java.util.List<Supplier<Scope>> Scope.continueIn() {
    return scopes;
  }
  public IDeclaration Scope.localLookup(String name) {
    return bindings.get(name);
  }

  syn IDeclaration IUse.decl() = lookup(name());
  public IDeclaration IUse.lookup(String name) {
    Set<Scope> visited = new HashSet<>();
    IDeclaration decl = scope().lookup(name, visited);
    return decl != null ? decl : declNotFound();
  }
  syn IDeclaration ASTNode.declNotFound() = null;

  public IDeclaration Scope.lookup(String name, Set<Scope> visited) {
    if (visited.contains(this)) {
      return null;
    }
    visited.add(this);
    IDeclaration d = localLookup(name);
    if (d != null) {
      return d;
    }
    for (Supplier<Scope> sup: continueIn()) {
      Scope s = sup.get();
      d = s.lookup(name, visited);
      if (d != null) {
        return d;
      }
    }
    return null;
  }

  inh Scope ASTNode.scope();
}

aspect Utils {
  public static <T> Supplier<T> ASTNode.cachedSupplier(Supplier<T> sup) {
    return new Supplier<T>() {
      private T v = null;
      private boolean computed = false;
      public T get() {
        if (!computed) {
          v = sup.get();
          computed = true;
        }
        return v;
      }
    };
  }
}
