import AST.*;

public class PrintAST extends Compiler {
	public static void main(String[] args) {
		new PrintAST().run(args);
	}

	public void runPreErrorCheck(Program p) {
		p.printAST();
	}		
}
