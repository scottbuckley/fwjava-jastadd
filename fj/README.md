# JastAdd Implementation of Featherweight Java

This is a JastAdd implementation of Featherweight Java (without generics). The name analysis has been specified in a JastAdd way and the type rules follow the paper more closely.

Run the compiler on an example program:

    $ ant jar
    $ java -jar fj-compiler.jar examples/pair.fj

Run test cases:

    $ ant test

Some of the test cases are adapted from the artifact of the paper:

- Hendrik van Antwerpen, Casper Bach Poulsen, Arjen Rouvoet, and Eelco Visser. 2018. Scopes as Types. Proc. ACM Program. Lang. 2, OOPSLA, Article 114 (November 2018), 30 pages. https://doi.org/10.1145/3276484. https://github.com/MetaBorgCube/oopsla18-artifact


# Other

Update expected files:

    $ for f in `ls testfiles/scope-paper/*.fj`; do java -jar fj-compiler.jar $f > "${f%.*}.expected"; done
