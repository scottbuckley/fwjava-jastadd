package org.jastadd.fj.compiler;

import org.jastadd.fj.compiler.FJParser.Terminals;

%%

%public
%final
%class FJScanner
%extends beaver.Scanner

%type beaver.Symbol
%function nextToken
%yylexthrow beaver.Scanner.Exception
%scanerror FJScanner.ScannerError

%line
%column
%{
  private beaver.Symbol sym(short id) {
    return new beaver.Symbol(id, yyline + 1, yycolumn + 1, yylength(), yytext());
  }

  private beaver.Symbol sym(short id, String text) {
    return new beaver.Symbol(id, yyline + 1, yycolumn + 1, yylength(), text);
  }

  public static class ScannerError extends Error {
    public ScannerError(String message) {
        super(message);
    }
  }
%}

WhiteSpace = [ ] | \t | \f | \n | \r | \r\n
TraditionalComment   = "/*" [^*] ~"*/" | "/*" "*"+ "/"
EndOfLineComment = "//" [^\n|\r|\r\n]*
Comment = {TraditionalComment} | {EndOfLineComment}

ID = [a-zA-Z$][a-zA-Z0-9$_]*

%%
<YYINITIAL> {
    {WhiteSpace}    { /* ignore */ }
    {Comment}       { /* ignore */ }

    // Keywords
    "class"         { return sym(Terminals.CLASS); }
    "extends"       { return sym(Terminals.EXTENDS); }
    "super"         { return sym(Terminals.SUPER); }
    "this"          { return sym(Terminals.THIS); }
    "return"        { return sym(Terminals.RETURN); }
    "new"           { return sym(Terminals.NEW); }

    // ID
    {ID}            { return sym(Terminals.ID); }

    "="             { return sym(Terminals.ASSIGN); }


    "{"             { return sym(Terminals.LBRA); }
    "}"             { return sym(Terminals.RBRA); }
    "("             { return sym(Terminals.LPAR); }
    ")"             { return sym(Terminals.RPAR); }
    ";"             { return sym(Terminals.SCOL); }
    ","             { return sym(Terminals.COMMA); }
    "."             { return sym(Terminals.DOT); }


    // Literals
    <<EOF>>         { return sym(Terminals.EOF); }
}


[^]             { throw new ScannerError((yyline+1) +"," + (yycolumn+1) + ": Illegal character <"+yytext()+">"); }
