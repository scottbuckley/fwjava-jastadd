import ast.*;

public class Main {

	public static void main(String args[]) {
		records();
	}

	public static IdExpr newId(String id) {
		return new IdExpr(new IdUse(id));
	}

	public static void subtypes1() {
		// {b: num, a: num}
		RecordType rt1 = new RecordType(
			new List().add(
				new RecordTypeEntry(new IdDecl("b"), new NumType())
			).add(
				new RecordTypeEntry(new IdDecl("a"), new NumType())
			)
		);
		// {b: num}
		RecordType rt2 = new RecordType(
			new List().add(
				new RecordTypeEntry(new IdDecl("b"), new NumType())
			)
		);
		System.out.println(rt1 + " <: " + rt2 + " = " + rt1.isSubtypeOf(rt2));
		System.out.println(rt2 + " <: " + rt1 + " = " + rt2.isSubtypeOf(rt1));
	}

	public static void with() {
		/**
		 * with {b = 1, a = 2} do a + b
		 */
		Record r = new Record(
			new List().add(
				new RecordEntry(new IdDecl("b"), new Literal(1))
			).add(
				new RecordEntry(new IdDecl("a"), new Literal(2))
			)
		);
		Addition add = new Addition(
			newId("a"),
			newId("b")
		);
		With with = new With(r, add);
		Program p = new Program(with);

		System.out.println(p);
		System.out.println("typeOf(Program) = " + p.type());
		System.out.println("typeOf(" + add.getLeft() + ") = " + add.getLeft().type());
		System.out.println("typeOf(" + add.getRight() + ") = " + add.getRight().type());

		p.printAST();
	}


	public static void records() {
		/**
		* let r = {a = 23, b = {...}} in
		* let q = {b = 19} extends r in
		* let f = fun (p : {b : num}) { p.b } in
		* f q + q.b
		*/
		App addLeft = new App(newId("f"), newId("q"));
		Access addRight = new Access(new IdUse("b"), newId("q"));
		Addition add1 =  new Addition(
			addLeft,
			addRight
		);
		Fun f1 = new Fun(
			new IdDecl("p"),
			new RecordType(
				new List().add(
					new RecordTypeEntry(new IdDecl("b"), new NumType()))
			),
			new Access(new IdUse("b"), newId("p"))
		);
		Let lf = new Let(
			new IdDecl("f"),
			f1,
			add1
		);

		// let q = {b = 19} extends r in
		Extends e = new Extends(
			new Record(
				new List().add(
					new RecordEntry(new IdDecl("b"), new Literal(19))
				)
			),
			newId("r")
		);
		Let letq = new Let(
			new IdDecl("q"),
			e,
			lf
		);

		// let r = {a = 23, b = {...}} in
		Record rb = new Record(
			new List().add(
				new RecordEntry(new IdDecl("z"), new Literal(20))
			)
		);
		Record rr = new Record(
			new List().add(
				new RecordEntry(new IdDecl("a"), new Literal(23))
			).add(
				new RecordEntry(new IdDecl("b"), rb)
			)
		);
		Let lr = new Let(
			new IdDecl("r"),
			rr,
			letq
		);

		Program p = new Program(lr);

		System.out.println(p);
		System.out.println("typeOf(Program) = " + p.type());
		System.out.println("typeOf(" + f1 + ") = " + f1.type());
		System.out.println("typeOf(" + e + ") = " + e.type());
		System.out.println("typeOf(" + addLeft + ") = " + addLeft.type());
		System.out.println("typeOf(" + addLeft.getLeft() + ") = " + addLeft.getLeft().type());
		System.out.println("typeOf(" + addLeft.getRight() + ") = " + addLeft.getRight().type());
	}

	public static void t1() {
		/**
 		 * let x = 3 in
		 * let f = fun(x : num) { x } in
		 * f x
		 */
		// Build bottom-up
		App a1 = new App(
			new IdExpr(new IdUse("f")),
			new IdExpr(new IdUse("x")));
		Fun f1 = new Fun(
			new IdDecl("x"),
			new NumType(),
			new IdExpr(new IdUse("x")));
		Let l1 = new Let(
			new IdDecl("f"),
			f1,
			a1);
		Let l2 = new Let(
			new IdDecl("x"),
			new Literal(3),
			l1);
		Program p = new Program(l2);

		System.out.println(p);

		p.printAST();
	}
}
