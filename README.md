# FWJava-JastAdd

The Featherweight Java implementation is in the subfolder [fj](fj).

A full README is available [in that directory](fj/README.md), but all you need to know to run this example is as follows.

Run the compiler on an example program:

    $ ant jar
    $ java -jar fj-compiler.jar examples/pair.fj

Run test cases:

    $ ant test